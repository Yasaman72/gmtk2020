﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FindPlayer
{
    public static GameObject GetPlayer()
    {
        var player = GameObject.FindWithTag("Player");

        if (player == null)
        {
            Debug.LogWarning("couldn't find the player");
        }
        
        return player;
    }
}
