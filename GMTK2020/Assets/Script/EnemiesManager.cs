﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesManager : MonoBehaviour
{
    public delegate void GrabbedPlayer();

    public static event GrabbedPlayer grabbedPlayer;

    public static void InvokeGrabEvent()
    {
        grabbedPlayer();
    }
}