﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyGrab : MonoBehaviour
{
    [SerializeField] private UnityEvent onGrab;
    [SerializeField] private bool drawGizmos= true;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject enemyNest;
    [SerializeField] private EnemyFollow enemyFollow;
    [SerializeField] private float moveToNestDelay = 1;

    [HideInInspector] public Enemy enemy;
    [HideInInspector] public CursorController cursorController;
    private bool grabbing;
    private GameObject player;



    private void Start()
    {
        player = FindPlayer.GetPlayer();
        cursorController = player.GetComponent<CursorController>();
        enemy = transform.parent.GetComponent<Enemy>();
    }

    private void OnEnable()
    {
        EnemiesManager.grabbedPlayer += OnGrabbedPlayerCalled;
    }

    private void OnDisable()
    {
        EnemiesManager.grabbedPlayer -= OnGrabbedPlayerCalled;
    }

    private void OnGrabbedPlayerCalled()
    {
        if (cursorController.grabbingEnemy != enemy)
        {
            enemyFollow.ChangeFollowState(false);
            animator.SetBool("close", false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if(cursorController.Grabbed) return;
            
            cursorController.Grabbed = true;
            cursorController.grabbingEnemy = enemy;
            animator.SetBool("grabbing", true);

            grabbing = true;
            StartCoroutine(MovePlayer());
            
            onGrab.Invoke();

            EnemiesManager.InvokeGrabEvent();
            Invoke(nameof(MovePlayerToNest), moveToNestDelay);
        }
    }

    private IEnumerator MovePlayer()
    {
        while (grabbing)
        {
            player.transform.position = transform.position;
            yield return new WaitForEndOfFrame();
        }
    }

    private void MovePlayerToNest()
    {
        if (!enemyNest) return;

        enemyFollow.ChangeFollowTarget(enemyNest);
    }

    private void OnDrawGizmos()
    {
        if (!drawGizmos) return;
        
        if (enemyNest != null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, enemyNest.transform.position);
        }
    }
}