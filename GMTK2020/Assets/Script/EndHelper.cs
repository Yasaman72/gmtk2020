﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EndHelper : MonoBehaviour
{

    [SerializeField] private UnityEvent onEnd;

    public void OnEndCall()
    {
        onEnd.Invoke();
    }
}
