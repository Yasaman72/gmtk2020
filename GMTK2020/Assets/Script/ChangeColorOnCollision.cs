﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnCollision : MonoBehaviour
{
    private SpriteRenderer SpriteRenderer;

    private void Start()
    {
        SpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            SpriteRenderer.color = Random.ColorHSV();
        }
    }
}
