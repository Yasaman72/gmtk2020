﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;

public class FallingObstacle : MonoBehaviour
{
    [SerializeField] private Collider2D[] otherColldiers;
    [SerializeField] private Collider2D myCollider;
    [Space]
    [SerializeField] private GameObject effectOnContactPoint;
    [SerializeField] private UnityEvent onFall;
    [SerializeField] private float giveBackControlAfter = 2;
    private ContactPoint2D[] contacts = new ContactPoint2D[2];

    private GameObject player;

    
    private void Start()
    {
        foreach (var col in otherColldiers)
        {
            Physics2D.IgnoreCollision(myCollider, col, true);
        }
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetContacts(contacts);
            if (effectOnContactPoint)
                Instantiate(effectOnContactPoint, contacts[0].point, quaternion.identity);
            onFall.Invoke();
            
            other.GetComponent<CursorController>().fall = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            player = other.gameObject;
            Invoke(nameof(GainBackControl), giveBackControlAfter);
        }
    }

    private void GainBackControl()
    {
        Vector2 pos =
            Camera.main.WorldToScreenPoint(new Vector3(player.transform.position.x,
                player.transform.position.y));
        HardwareCursor.SetLocalPosition(pos);

        player.GetComponent<CursorController>().fall = false;
    }
}