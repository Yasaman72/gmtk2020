﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMover : MonoBehaviour
{
    [SerializeField] private Vector2 speed;

    private void OnEnable()
    {
        GetComponent<SpriteRenderer>().material.SetVector("_ScrollSpeed", speed);

    }

}
