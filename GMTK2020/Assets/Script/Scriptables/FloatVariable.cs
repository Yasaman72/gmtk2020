﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "Custom Framework/Variables/Float", order = 1)]
public class FloatVariable : ScriptableVar<float>
{
    public void AddNewValue(float newAmount)
    {
        RuntimeValue = RuntimeValue + newAmount;
    }

    public override void SaveValue(float value)
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        if (_debug)
        {
            Debug.Log("saved data for: " + id);
        }

        PlayerPrefs.SetFloat(id, value);
    }

    protected override float GetValue()
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        if (_debug)
        {
            Debug.Log("getting data for: " + id);
        }

        return PlayerPrefs.GetFloat(id);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(FloatVariable)), CanEditMultipleObjects]
public class FloatVar : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Space(30);

        FloatVariable myScript = (FloatVariable) target;
        if (GUILayout.Button("Delete playerPref Key"))
        {
            myScript.DeletePlayerPrefKey();
        }
    }
}

#endif