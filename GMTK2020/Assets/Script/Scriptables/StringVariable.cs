﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "StringVariable", menuName = "Custom Framework/Variables/String", order = 1)]
public class StringVariable : ScriptableVar<string>
{
    public void SetString(string String)
    {
        RuntimeValue = String;
    }

    public override void SaveValue(string value)
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        PlayerPrefs.SetString(id, value);
        if (_debug)
        {
            Debug.Log("saved data for: " + id);
        }
    }

    protected override String GetValue()
    {
        string id = playerPrefId +"_"+ GetInstanceID();

        if (_debug)
        {
            Debug.Log("getting data for: " + id);
        }
        return PlayerPrefs.GetString(id);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(StringVariable)), CanEditMultipleObjects]
public class Stringvar : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Space(30);

        StringVariable myScript = (StringVariable) target;
        if (GUILayout.Button("Delete playerPref Key"))
        {
            myScript.DeletePlayerPrefKey();
        }
    }
}

#endif