﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoolVarListener : MonoBehaviour
{
    public BoolVariable Variable;
    public bool checkInStart;
    public float checkInStartDelay;
    public UnityEvent onTrue;
    public UnityEvent onFalse;

    private void OnEnable()
    {
        Variable.AddListener(OnEventRaised);
    }

    private void OnDisable()
    {
        Variable.RemoveListener(OnEventRaised);
    }


    private void Start()
    {
        if (checkInStart)
            Invoke(nameof(OnEventRaised), checkInStartDelay);
    }

    public void OnEventRaised()
    {
        if (Variable.RuntimeValue)
        {
            onTrue.Invoke();
        }
        else
        {
            onFalse.Invoke();
        }
    }
}
