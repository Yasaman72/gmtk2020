﻿using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class ManagedObject : ScriptableObject
{
    [SerializeField] private UnityEvent _onBeginEvent;
    [SerializeField] private UnityEvent _onEndEvent;
    [SerializeField] private UnityEvent _onEnableEvent;
    [SerializeField] private UnityEvent _onDisableEvent;

#if UNITY_EDITOR
    protected virtual void OnEnable()
    {
        EditorApplication.playModeStateChanged += OnPlayStateChange;
        _onEnableEvent?.Invoke();
    }

    protected virtual void OnDisable()
    {
        EditorApplication.playModeStateChanged -= OnPlayStateChange;
        _onDisableEvent?.Invoke();
    }

    private void OnPlayStateChange(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredPlayMode)
        {
            OnAppStart();
        }
        else if (state == PlayModeStateChange.ExitingPlayMode)
        {
            OnAppEnd();
        }
    }
#else
        protected virtual void OnEnable()
        {
            OnAppStart();
            _onEnableEvent?.Invoke();
        }
 
        protected virtual void OnDisable()
        {
            OnAppEnd();
            _onDisableEvent?.Invoke();
        }
#endif



    protected virtual void OnAppStart()
    {
        _onBeginEvent?.Invoke();
    }

    protected virtual void OnAppEnd()
    {
        _onEndEvent?.Invoke();
    }
}