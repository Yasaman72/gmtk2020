﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TimeVariable", menuName = "Custom Framework/Variables/Time", order = 1)]
public class TimeVariable : ScriptableVar<System.DateTime>
{
    
}
