﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Vector3Variable", menuName = "Custom Framework/Variables/Vector3", order = 1)]
public class Vector3Variable : ScriptableObject
{
    public Vector3 _initialValue;

    public Vector3 _runtimeValue;

    private void OnEnable()
    {
        _runtimeValue = _initialValue;
    }

    public void SetValue(Vector3 value)
    {
        _runtimeValue = value;
    }

    public Vector3 GetValue()
    {
        return _runtimeValue;
    }
}
