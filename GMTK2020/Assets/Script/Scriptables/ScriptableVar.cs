﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ScriptableVar<T> : ManagedObject
{
    [SerializeField, TextArea(2, 10)] private string _note;
    [Space(20)] public bool _debug;
    public bool _setOnTheSameValue = true;
    [Space] public bool _persistence;
    public string playerPrefId;
    [SerializeField] private List<UnityAction> _actions = new List<UnityAction>();
    private bool _isInit;
    [Space] public T _initialValue;
    [SerializeField] private T _runtimeValue;

    public T RuntimeValue
    {
        get
        {
            if (_persistence)
            {
                return GetValue();
            }
            else
            {
                return _isInit ? _runtimeValue : _initialValue;
            }
        }

        set
        {
            if (!_isInit)
                _isInit = true;

            if (!_setOnTheSameValue && EqualityComparer<T>.Default.Equals(value, _runtimeValue))
                return;

            _runtimeValue = value;

            if (_debug)
            {
                Debug.Log(name + "runtime values is: " + _runtimeValue);
            }

            if (_persistence)
                SaveValue(value);

            foreach (var act in _actions.ToList())
            {
                act();
            }

            //PlayerPrefs.SetString(this.GetInstanceID().ToString(), value.ToString());
        }
    }


    protected override void OnAppStart()
    {
        if (!_isInit)
        {
            if (_persistence)
            {
                RuntimeValue = GetValue();
            }
            else
            {
                _runtimeValue = _initialValue;
            }

            base.OnAppStart();
            _isInit = true;
        }
    }

    public void AddListener(UnityAction func)
    {
        if (_actions.Contains(func)) return;
        _actions.Add(func);
    }

    public void RemoveListener(UnityAction func)
    {
        if (!_actions.Contains(func)) return;
        _actions.Remove(func);
    }

    private void OnDestroy()
    {
        _actions.Clear();
    }

    protected override void OnEnable()
    {
        if (_persistence)
        {
            RuntimeValue = GetValue();
        }
    }

    private void OnValidate()
    {
        if (!_persistence)
        {
            RuntimeValue = _initialValue;
        }
    }

    public virtual void SaveValue(T value)
    {
    }

    protected virtual T GetValue()
    {
        return default;
    }

    public void DeletePlayerPrefKey()
    {
        if (!String.IsNullOrEmpty(playerPrefId))
        {
            string id = playerPrefId + "_" + GetInstanceID();

            if (PlayerPrefs.HasKey(id))
            {
                PlayerPrefs.DeleteKey(id);
                Debug.LogWarning("deleted playerpref for: " + id);
            }
            else
            {
                Debug.LogWarning("find no key for: " + id);
            }
        }
    }
}