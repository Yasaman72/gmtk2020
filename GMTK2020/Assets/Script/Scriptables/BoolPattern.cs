﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BoolPattern : ScriptableObject
{
    public bool[] pattern;
}
