﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    [SerializeField] private GameObject EndScreen;

    public void EndTheGame()
    {
        EndScreen.SetActive(true);
    }

    public void ExitGame()
    {
        Cursor.visible = true;
        Application.Quit();
    }
}