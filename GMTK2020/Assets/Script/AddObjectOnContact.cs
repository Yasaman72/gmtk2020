﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AddObjectOnContact : MonoBehaviour
{
    [SerializeField] private GameObject objectToGenerate;
    [SerializeField] private UnityEvent onContact;
    
    private ContactPoint2D[] contacts = new ContactPoint2D[2];
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(!objectToGenerate) return;
        if(!other.gameObject.CompareTag("Player")) return;
        
        other.GetContacts(contacts);
        Instantiate(objectToGenerate, contacts[0].point, Quaternion.identity);
        
        onContact.Invoke();
    }
}
