﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class BouncingObstacle : MonoBehaviour
{
    [SerializeField] private GameObject objectWithPOintForce;
    private ContactPoint2D[] contacts = new ContactPoint2D[2];

    private void Start()
    {
        MoveToOrigin();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(!other.gameObject.CompareTag("Player")) return;
        
        other.GetContacts(contacts);

        objectWithPOintForce.transform.position = contacts[0].point;
        Invoke(nameof(MoveToOrigin), 1);
    }

    private void MoveToOrigin()
    {
        objectWithPOintForce.transform.position = transform.position;
    }
}