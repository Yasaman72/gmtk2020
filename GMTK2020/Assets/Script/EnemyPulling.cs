﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPulling : MonoBehaviour
{
    [SerializeField] private float pullForce = 10;
    [SerializeField] private float pullDistance = 10;

    private bool pull;
    private GameObject player;
    private CursorController cursorController;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        cursorController = player.GetComponent<CursorController>();
    }


    private void FixedUpdate()
    {
        if (Vector2.Distance(transform.position, player.transform.position) < pullDistance)
        {
            Vector2 dir =  transform.position - player.transform.position;
            cursorController.rb.AddForce(dir.normalized * pullForce);
        }
       
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,pullDistance);
    }
}
