﻿using UnityEngine;

public class CursorController : MonoBehaviour
{
    [HideInInspector] public bool followMouseInput = true;
    [HideInInspector] public Enemy grabbingEnemy;
    private bool _fall;

    public bool fall
    {
        get => _fall;
        set
        {
            _fall = value;
            rb.gravityScale = fall ? gravityScale : 0;
        }
    }

    private bool _grabbed;

    public bool Grabbed
    {
        get => _grabbed;
        set
        {
            _grabbed = value;
            followMouseInput = !_grabbed;
        }
    }

    public float gravityScale = 0.5f;
    [SerializeField] private Vector2 mouseInitialPosition;
    [SerializeField] public Rigidbody2D rb;
    [SerializeField] public bool isRealCursorVisible;
    [SerializeField] private float initialSpeed = 20;

    private float initialDrag;
    private Camera camera;
    private Vector2 followTarget;
    private GameObject followObj;
    private Vector3 dir;
    private float speed;

    
    private void Start()
    {
        speed = initialSpeed;
        initialDrag = rb.drag;
        camera = Camera.main;

        HardwareCursor.SetLocalPosition(mouseInitialPosition);
        
        // move cursor to the starting position of the fake cursor
//        HardwareCursor.SetLocalPosition(camera.WorldToViewportPoint(transform.position));
    }

    void FixedUpdate()
    {
        Cursor.visible = isRealCursorVisible;

        if (fall)
            return;

        if (followMouseInput)
        {
            dir = followTarget - (Vector2) transform.position;
            rb.AddForce(dir * speed);
//          rb.MovePosition(followTarget * speed);
            ChangeFollowTarget(camera.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    public void ChangeSpeed(bool toOriginal = false, float newSpeed = 1)
    {
        speed = toOriginal ? initialSpeed : newSpeed;
    }

    public void ChangeDrag(bool toOriginal = false, float newDrag = 1)
    {
        rb.drag = toOriginal ? initialDrag : newDrag;
    }

    public void ChangeFollowTarget(Vector2 newTargetPos)
    {
        followTarget = newTargetPos;
    }
}