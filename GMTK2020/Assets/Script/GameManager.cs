﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] private bool changeMusicOnEnteredLevel;
    [SerializeField] private int musicIndex;
    [Space] [SerializeField] private SpriteRenderer fadeScreen;
    [SerializeField] private float screenFadeDuration = 1;

    public delegate void OnGameOver();

    public static event OnGameOver gameOver;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        if (changeMusicOnEnteredLevel)
        {
            if (MusicManager.instance)
                MusicManager.instance.PlayMusic(musicIndex);
        }

        fadeScreen.gameObject.SetActive(true);
        Tween tween = fadeScreen.DOFade(0, screenFadeDuration);
        tween.onComplete = AfterOnStartFadeIn;
    }

    private void AfterOnStartFadeIn()
    {
        fadeScreen.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void OnEnable()
    {
        gameOver += GameOver;
    }

    private void OnDisable()
    {
        gameOver -= GameOver;
    }

    public void InvokeGameOver()
    {
        gameOver.Invoke();
    }

    private void GameOver()
    {
        fadeScreen.gameObject.SetActive(true);
        Tween tween = fadeScreen.DOFade(1, screenFadeDuration);
        tween.onComplete = OnFadeEnd;
    }

    private void OnFadeEnd()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}