﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioPitch : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private float pitchMin, pitchMax;


    public void RandomizePitch()
    {
        audioSource.pitch = Random.Range(pitchMin, pitchMax);
    }
}