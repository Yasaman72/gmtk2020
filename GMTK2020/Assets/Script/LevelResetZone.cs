﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelResetZone : MonoBehaviour
{

    [SerializeField] private UnityEvent onEntered;
    [SerializeField] private float restartAfter = 1;

 

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.SetActive(false);
            onEntered.Invoke();
            Invoke(nameof(Restart), restartAfter);
        }
    }

    private void Restart()
    {
        GameManager.instance.InvokeGameOver();
    }
}