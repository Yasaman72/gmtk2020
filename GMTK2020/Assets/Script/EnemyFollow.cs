﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    [SerializeField] private bool ignorePlayerCollider = true;
    [SerializeField] private Collider2D collider2;

    [Space] [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float speed;
    [SerializeField] private Animator animator;
    private bool follow;

    private GameObject FollowTarget;
    private CursorController _cursorController;
    private GameObject player;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        _cursorController = player.GetComponent<CursorController>();

        if (ignorePlayerCollider)
        {
            if (player.GetComponent<Collider2D>())
                Physics2D.IgnoreCollision(collider2, player.GetComponent<Collider2D>(), true);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if(_cursorController.Grabbed) return;
            follow = true;
            ChangeFollowTarget(other.gameObject);
            animator.SetBool("close", true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            follow = false;
            animator.SetBool("close", false);
        }
    }

    private void Update()
    {
        if (follow)
        {
            var dir = FollowTarget.transform.position - transform.position;
            rb.AddForce(dir * speed);
        }
    }

    public void ChangeFollowTarget(GameObject newTarget)
    {
        FollowTarget = newTarget;
    }

    public void ChangeFollowState(bool shouldFollow)
    {
        follow = shouldFollow;
    }
}