﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private int initialMusicIndex;
    [SerializeField] private AudioClip[] musics;

    private float initialvolume;
    private int soundIndex;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        initialvolume = audioSource.volume;
        
        soundIndex = initialMusicIndex;
        audioSource.clip = musics[soundIndex];
        audioSource.Play();
    }

    public void PlayMusic(int index)
    {
        if (index == soundIndex) return;
        
        soundIndex = index;
        
        Tween tween = audioSource.DOFade(0, 0.5f);
        tween.onComplete = OnFadeEnd;
    }

    private void OnFadeEnd()
    {
        audioSource.clip = musics[soundIndex];
        audioSource.DOFade(initialvolume, 0.5f);
        audioSource.Play();
    }
}