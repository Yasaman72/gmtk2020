﻿using UnityEngine;
using UnityEditor;

public class GameControllerEditor : EditorWindow
{
    [MenuItem("Custom/Game Controller")]

    public static void ShowWindow()
    {
        GetWindow<GameControllerEditor>("Game Controller");
    }

    void OnGUI()
    {
        GUILayout.Label("Delete All", EditorStyles.largeLabel);


        if (GUILayout.Button("Delete All"))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Deleted all");
        }
    }
}